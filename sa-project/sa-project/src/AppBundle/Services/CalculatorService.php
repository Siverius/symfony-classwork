<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/23/2019
 * Time: 4:15 PM
 */

namespace AppBundle\Services;


class CalculatorService
{
    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    public function sum($a, $b)
    {
        return $a + $b;
    }

    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    public function div($a, $b)
    {
        return $a - $b;
    }
}
