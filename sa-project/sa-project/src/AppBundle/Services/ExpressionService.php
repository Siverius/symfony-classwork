<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/23/2019
 * Time: 4:20 PM
 */

namespace AppBundle\Services;


class ExpressionService
{

    private $improver = 3;
    private $calculator;

    /**
     * ExpressionService constructor.
     * @param CalculatorService $calculator
     */
    public function __construct(CalculatorService $calculator)
    {
        $this->calculator = $calculator;
    }

    public function calculate($a, $b, $c)
    {
        return $this->calculator->sum($a, $b) + $this->calculator->sum($b, $c) + $this->improver;
    }
}
