<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/6/2019
 * Time: 4:38 PM
 */

namespace AppBundle\Validator\Constraints;

use \DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NotEarlierThanTodayValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $now = new DateTime();

        if($value < $now) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value->format("Y-m-d"))
                ->addViolation();
        }
    }
}