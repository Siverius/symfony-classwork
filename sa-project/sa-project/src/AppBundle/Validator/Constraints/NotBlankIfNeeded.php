<?php


namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class NotBlankIfNeeded extends Constraint
{
    public $message = "This value should not be blank.";
    public $isNeeded;

    public function __construct($isNeeded, $options = null)
    {
        parent::__construct($options);
        $this->isNeeded = $isNeeded;
    }
}