<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/6/2019
 * Time: 4:35 PM
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class NotEarlierThanToday extends Constraint
{
    public $message = 'Date must be today or later. Date "{{ value }}" didn`t pass';
}