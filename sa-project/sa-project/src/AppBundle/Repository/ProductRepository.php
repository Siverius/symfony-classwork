<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/4/2019
 * Time: 4:37 PM
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findAllByNameAndPrice($name, $price)
    {
        $repository = $this->getEntityManager()->getRepository(Product::class);
        $query = $repository->createQueryBuilder('p')
            ->where('p.name = :name')
            ->andWhere('p.price = :price')
            ->setParameters([
                ':name' => $name,
                ':price' => $price
            ])
            ->getQuery();

        return $query->getResult();
    }
}