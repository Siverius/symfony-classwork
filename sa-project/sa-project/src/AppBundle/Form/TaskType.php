<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/6/2019
 * Time: 3:02 PM
 */

namespace AppBundle\Form;

use AppBundle\Validator\Constraints\NotEarlierThanToday;
use AppBundle\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'task.new.title',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'label' => 'task.new.description',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add(
                'dueDate',
                DateType::class,
                [
                    'label' => 'task.new.dueDate',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotEarlierThanToday()
                    ]
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'task.new.save'
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
