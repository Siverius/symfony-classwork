<?php

namespace AppBundle\Form;

use AppBundle\Validator\Constraints\NotBlankIfNeeded;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isNewRecord = $options['data']->isNewRecord;

        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'label.user.email'
                ]
            )
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'label.user.username',
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add(
                'password',
                PasswordType::class,
                [
                    'label' => 'label.user.password',
                    'required' => $isNewRecord,
                    'constraints' => [
                        new NotBlankIfNeeded($isNewRecord)
                    ]
                ]
            )
            ->add('save', SubmitType::class);

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
