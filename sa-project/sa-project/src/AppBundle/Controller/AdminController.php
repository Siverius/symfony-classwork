<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route(
 *     "/",
 *     name="admin_",
 *     host="{admin_subdomain}.{domain}",
 *     requirements={
 *         "admin_subdomain"="%admin_subdomain%",
 *         "domain"="%domain%"
 *     },
 *     defaults={
 *         "admin_subdomain"="%admin_subdomain%",
 *         "domain"="%domain%"
 *     }
 * )
 *
 * Class AdminController
 * @package AppBundle\Controller
 */

class AdminController extends Controller
{
    /**
     * @Route("/", name="index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $my_string = "Me is string. Dump me!";
        $my_integer = 42;
        $my_array = [
            'red',
            'orange',
            'yellow',
            'green',
            'cyan',
            'blue',
            'violet'

        ];


        return $this->render('admin/index.html.twig',
            [
                'my_string' => $my_string,
                'my_integer' => $my_integer,
                'my_array' => $my_array,
                'my_class' => $this,
            ]
        );
    }

    /**
     * @Route("/code", name="admin_code")
     *
     **/
    public function codeAction()
    {
        $code = '<script>alert("Hello");</script>';

        return $this->render('admin/code.html.twig',
            [
                'code' => $code,
            ]
        );
    }
    /**
     * @Route("/trans/{_locale}/{count}", name="trans", defaults={"_locale": "en"})
     *
     **/
    public function transAction(TranslatorInterface $translator, $count)
    {
        $name = 'Mario Piper';
        $message = $translator->trans("Hello %name%", ['%name%' => $name]);
        $goods = $translator->transChoice('add_to_cart', $count);

        return $this->render('admin/trans.html.twig',
            [
                'message' => $message,
                'goods' => $goods,
            ]
        );
    }
}
