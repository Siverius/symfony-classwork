<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class PrivacyController extends Controller
{
    /**
     * @Route("/privacy/policy/{name}", name="named-privacy-policy", requirements={"name"="[a-zA-Z]+"})
     * @Route("/privacy/policy/{name}", name="named-privacy-policy", requirements={"name"="[a-zA-Z]+"})
     */
    public function namedPolicyAction($name)
    {
        return new Response(
            "<html>Privacy Policy with $name 
                    <p>THIS IS namedPolicyAction</p></html>"
        );
    }

    /**
     * @Route("/privacy/policy/{count}", name="counted-privacy-policy", requirements={"count"="\d+"})
     */
    public function countedPolicyAction($count)
    {
        if ($count == 7) {
            throw $this->createNotFoundException();
        }
        return new Response(
            "<html>Privacy Policy with number: $count 
                    <p>THIS IS countedPolicyAction</p></html>"
        );
    }

    /**
     * @Route("/privacy/policy", name="privacy-policy")
     */
    public function policyAction(Request $request)
    {
        $name = $request->get("name", "Smith");
        $this->addFlash('success', "My name is Neo");
        return new Response(
            "<html>
                        <body>
                            Hello, mr $name
                        </body>
                    </html>"
        );
    }

    /**
     * @Route("/session/set/{answer}", name="session-set")
     */
    public function setSessionAction($answer, SessionInterface $session)
    {
        $session->set('answer', $answer);
        return $this->redirectToRoute("session-get");
    }

    /**
     * @Route("/session/get", name="session-get")
     */
    public function getSessionAction(SessionInterface $session)
    {
        $answer = $session->get("answer");
        return new Response(
            "Answer is $answer"
        );
    }

    /**
     * @Route("/hello", name="hello")
     */
    public function helloAction()
    {
        $today = date("Y-m-d H:i:s");
        $clients = [
            ['name' => 'Thomas', 'surname' => 'Anderson', 'phone' => '+375(29) 555-66-77', 'dateOfBirth' => '1975-02-18'],
            ['name' => 'Morpheus', 'surname' => 'Theonesearcher', 'phone' => '+375(29) 666-77-88', 'dateOfBirth' => '1963-11-08'],
            ['name' => 'Trinity', 'surname' => 'Neogirl', 'phone' => '+375(29) 777-88-99', 'dateOfBirth' => '1981-12-15'],
            ['name' => 'Tank', 'surname' => 'Driver', 'phone' => null, 'dateOfBirth' => '2123-08-09'],
            ['name' => 'Dozer', 'surname' => 'Driver', 'phone' => null, 'dateOfBirth' => '2125-01-05'],
        ];

        return $this->render('clients/index.html.twig',
            [
                'clients' => $clients,
                'today' => $today,
            ]
        );
    }
}
