<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Services\ExpressionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            'sum' => "hellO"
        ]);
    }

    /**
     * @Route("/test", name="testpage")
     */
    public function testAction()
    {
        $expression = $this->container->get(ExpressionService::class);
        $sum = $expression->calculate(1, 2, 3);
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            'sum' => $sum,
        ]);
    }

    /**
     * @Route("/create-product/{productName}", name="create_product")
     * @param $productName
     * @return Response
     */
    public function createAction($productName)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->setName($productName);
        $product->setPrice(10);
        $product->setDescription('Stylish!');

        $entityManager->persist($product);

        $entityManager->flush();

        return $this->render('default/create.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/show-product/{productName}", name="show-product")
     */
    public function showAction($productName)
    {
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneByName($productName);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found with name ' . $productName
            );
        }

        return $this->render('default/show.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/update-product/{productId}", name="update-product")
     */
    public function updateAction($productId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($productId);

        if(!$product) {
            throw $this->createNotFoundException(
                'No product found for id ' . $productId
            );
        }
        $oldProduct = clone $product;
        $product->setName('product');
        $product->setDescription('description');
        $product->setPrice(10);
        $entityManager->flush();

        return $this->render('default/update.html.twig', [
            'oldProduct' => $oldProduct,
            'product' => $product
        ]);
    }

    /**
     * @Route("list-products/{name}/{price}", name="list-products")
     * @param $name
     * @param $price
     * @return Response
     */
    public function listAction($name, $price)
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAllByNameAndPrice($name, $price);

        if(!$products) {
            throw $this->createNotFoundException(
                'No product founded'
            );
        }

        return $this->render('default/list.html.twig', ['products' => $products]);
    }
}
