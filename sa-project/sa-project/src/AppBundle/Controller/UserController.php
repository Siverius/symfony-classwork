<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends Controller
{

    /**
     * @Route("/new", name="new", methods={"POST", "GET"})
     * @Route("/{id}/edit", name="id_edit", requirements={"id"="\d+"}, methods={"POST", "GET"})
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function editAction(Request $request, UserPasswordEncoderInterface $encoder, $id = null)
    {
        $user = (null === $id) ? new User() : $this->findEntity($id);
        $form = $this->createForm(UserType::class, $user);
        $password = $user->getPassword();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if($data->getPassword() !== null) {
                $password = $encoder->encodePassword($user, $data->getPassword());
            }
            $data->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();

            $this->addFlash('success', "flash.admin.user.edit.success");
        }

        return $this->render('user/edit.html.twig',
            [
                'form' => $form->createView(),
                'user' => $user,
            ]
        );
    }
}
